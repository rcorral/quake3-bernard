-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: 10.0.1.71
-- Generation Time: Nov 08, 2012 at 02:03 PM
-- Server version: 5.5.28-0ubuntu0.12.04.2
-- PHP Version: 5.3.15

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `quakie`
--

-- --------------------------------------------------------

--
-- Table structure for table `maps`
--

DROP TABLE IF EXISTS `maps`;
CREATE TABLE IF NOT EXISTS `maps` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `size` enum('s','m','l','g') NOT NULL DEFAULT 'm' COMMENT 'categorization of map size: small, medium, large, giant',
  `gravity` int(11) NOT NULL DEFAULT '800',
  `count` int(11) unsigned NOT NULL DEFAULT '0' COMMENT 'Count of how many times map has been used',
  PRIMARY KEY (`id`),
  KEY `size` (`size`,`count`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=50 ;

--
-- Dumping data for table `maps`
--

INSERT INTO `maps` (`id`, `name`, `size`, `gravity`, `count`) VALUES
(1, 'q3ctf1', 's', 800, 2),
(2, 'q3ctf2', 'm', 800, 1),
(3, 'q3ctf3', 'l', 800, 2),
(4, 'q3ctf4', 'm', 800, 1),
(5, 'templatic', 'l', 800, 1),
(6, 'sq3ctf1', 'm', 800, 1),
(7, 'spirit3ctfduel1', 'l', 800, 1),
(8, 'saintctf2', 's', 800, 2),
(9, 'rota3ctf2', 'l', 800, 1),
(10, 'rota3ctf1', 's', 800, 1),
(11, 'q3wc1', 'g', 800, 0),
(12, 'q3wc2', 'g', 800, 0),
(13, 'q3wc3', 'g', 800, 1),
(14, 'q3wc4', 'g', 800, 1),
(15, 'q3wc5', 'g', 800, 1),
(16, 'q3wc6', 'g', 800, 1),
(17, 'q3wc7', 'g', 800, 1),
(18, 'q3wc8', 'g', 800, 1),
(19, 'q3wcp1', 'm', 800, 1),
(20, 'q3wcp2', 'l', 800, 1),
(21, 'q3wcp3', 'l', 800, 1),
(22, 'q3wcp4', 'm', 800, 1),
(23, 'q3wcp5', 'l', 800, 1),
(24, 'q3wcp6', 'l', 800, 1),
(25, 'q3wcp7', 'm', 800, 1),
(26, 'q3wcp8', 'm', 800, 1),
(27, 'pn03', 'l', 800, 1),
(28, 'phantq3ctf1_rev', 'l', 800, 1),
(29, 'pasctf4', 'g', 800, 1),
(30, 'q3tourney6_ctf', 's', 800, 1),
(31, 'on•xctf5', 'm', 800, 1),
(32, 'nor3ctf1', 'l', 800, 1),
(33, 'mkbase', 'm', 800, 1),
(34, 'mikectf3', 'l', 800, 1),
(35, 'mikectf3temp', 'l', 800, 1),
(36, 'mapel4b', 'l', 800, 1),
(37, 'rjlctf2', 'g', 800, 1),
(38, 'rjlctf1', 'g', 800, 1),
(39, 'kellblack', 'l', 800, 1),
(40, 'bunker_ctf1', 'l', 800, 1),
(41, 'aecad', 'm', 800, 1),
(42, '13void', 'm', 800, 1),
(43, '12vast', 'm', 800, 1),
(44, '13stone', 'm', 800, 1),
(45, '13ground', 'm', 800, 1),
(46, '13dream', 'm', 800, 1),
(47, '13death', 'm', 800, 2),
(48, 'leafland', 'm', 800, 2),
(49, 'krctf01', 'm', 800, 2);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
