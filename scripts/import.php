#!/usr/bin/env php
<?php

/**
 * An example command line application built on the Joomla Platform.
 *
 * To run this example, adjust the executable path above to suite your operating system,
 * make this file executable and run the file.
 *
 * Alternatively, run the file using:
 *
 * php -f run.php
 *
 * Note, this application requires configuration.php and the connection details
 * for the database may need to be changed to suit your local setup.
 *
 * @package    Joomla.Examples
 * @copyright  Copyright (C) 2005 - 2011 Open Source Matters, Inc. All rights reserved.
 * @license    GNU General Public License version 2 or later; see LICENSE
 */

// Bootstrap the application.
require 'bootstrap.php';

class ImportApp extends JApplicationCli
{
	protected $db = null;

	public function __construct()
	{
		parent::__construct();

		$this->db = JFactory::getDBO();
	}

	/**
	 * Execute the application.
	 *
	 * @return  void
	 *
	 * @since   11.3
	 */
	public function execute()
	{
		jimport( 'joomla.filesystem.file' );
		$file = JFile::read( 'import-maps.txt' );

		if ( !$file || empty( $file ) ) {
			return;
		}

		$tuples = array();
		$maps = explode( "\n", $file );

		foreach ( $maps as $_map ) {
			if ( empty( $_map ) ) {
				continue;
			}

			$map = explode( ' ', $_map );

			$tuples[] = '( ' . $this->db->q( $map[0] ) . ','
				. $this->db->q( isset( $map[1] ) ? $map[1] : 'm' ) . ', 0 )';
		}

		$this->db->setQuery( 'INSERT INTO maps
			( `name`, `size`, `count` )
			VALUES ' . implode( ', ', $tuples ) );
		$return = $this->db->query();

		if ( !$return ) {
			throw new Exception( "Error importing maps." );
		} else {
			echo "Success!\n";
		}
	}
}

// Wrap the execution in a try statement to catch any exceptions thrown anywhere in the script.
try {
	// Instantiate the application object, passing the class name to JApplicationCli::getInstance
	// and use chaining to execute the application.
	JApplicationCli::getInstance('ImportApp')->execute();
}
catch (Exception $e)
{
	// An exception has been caught, just echo the message.
	fwrite(STDOUT, $e->getMessage() . "\n");
	exit($e->getCode());
}
