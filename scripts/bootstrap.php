<?php

// We are a valid Joomla entry point.
define('_JEXEC', 1);

// Setup the base path related constant.
define('JPATH_BASE', dirname(__FILE__));
define('JPATH_SITE', dirname(__FILE__));

require dirname( dirname( __FILE__ ) ) . '/joomla-platform/libraries/import.php';
