#!/usr/bin/env php
<?php
/**
 * An example command line application built on the Joomla Platform.
 *
 * To run this example, adjust the executable path above to suite your operating system,
 * make this file executable and run the file.
 *
 * Alternatively, run the file using:
 *
 * php -f run.php
 *
 * Note, this application requires configuration.php and the connection details
 * for the database may need to be changed to suit your local setup.
 *
 * @package    Joomla.Examples
 * @copyright  Copyright (C) 2005 - 2011 Open Source Matters, Inc. All rights reserved.
 * @license    GNU General Public License version 2 or later; see LICENSE
 */

// Bootstrap the application.
require 'bootstrap.php';

class ServerApp extends JApplicationCli
{
	protected $db = null;

	protected $defaults = array(
		'map_count' => 3,
		'sizes' => 's,m,l',
		'capturelimit' => 20,
		'ioquake_path' => '/usr/local/games/ioquake3',
		'repo_path' => '/usr/local/games/quake3/quake3'
		);

	public function __construct()
	{
		parent::__construct();

		$this->db = JFactory::getDBO();
		$this->defaults = (object) $this->defaults;
	}

	public function show_help()
	{
		$this->out( 'Help:' );
		$this->out( 'You may pass the map sizes that you would like to include in rotation.' );
		$this->out( 'You can also pass the amount of maps in rotation.' );
		$this->out( 'Example:' );
		$this->out( 'php -f start-server.php s,m,l,h 5' );
	}

	/**
	 * Execute the application.
	 *
	 * @return  void
	 *
	 * @since   11.3
	 */
	public function execute()
	{
		$options = $this->defaults;

		if ( !empty( $this->input->args ) ) {
			if (
				in_array( str_replace( '-', '', $this->input->args[0] ), array( 'h', 'help' ) )
			) {
				$this->show_help();
				return;
			}

			$options->sizes = $this->input->args[0];

			if ( isset( $this->input->args[1] ) ) {
				$options->map_count = (int) $this->input->args[1];
			}
		} else {
			$this->out();
			$this->out( 'Starting server with default settings...' );
		}

		$sizes = explode( ',', $options->sizes );

		if ( empty( $sizes ) ) {
			$sizes = explode( ',', $this->defaults->sizes );
		}

		foreach ( $sizes as &$size ) {
			$size = $this->db->q( $size );
		}

		$query = $this->db->getQuery( true )
			->select( '`id`, `name`, `gravity`' )
			->from( 'maps' )
			->where( '`size` IN (' .implode( ',', $sizes ). ')' )
			->order( '`count` ASC' )
			;
		$maps = $this->db->setQuery( $query, 0, $options->map_count )->loadObjectList();

		$out_string = '';
		foreach ( $maps as $_counter => $map ) {
			$count = $_counter;
			$out_string .= "set m{$count} \""
				. "capturelimit {$options->capturelimit}; "
				. "g_gravity {$map->gravity}; "
				. "map {$map->name}; "
				. "set nextmap vstr m"
					. ( ( ( $count + 1 ) == $options->map_count ) ? 0 : $count + 1 )
				. '"'
				. "\n"
				;

			$this->update_map_count( $map->id );
		}

		$out_string .= "vstr m0\n";

		$r = file_put_contents( $this->defaults->ioquake_path . '/baseq3/maps.cfg', $out_string );

		if ( !$r ) {
			$this->out( 'Error wrting maps config file!!' );
			die();
		}

		$this->out( 'Using this map config:' );
		$this->out( $out_string );

		// Storing maps for the record
		$record_string = "\n" . date( 'Y-m-d H:i:s' ) . "\n"
			. $out_string
			. "\n-----------------------------------------------------------------";

		$r = file_put_contents(
			$this->defaults->repo_path . '/scripts/maps_record.log', $record_string, FILE_APPEND );

		if ( !$r ) {
			$this->out( 'Error wrting maps record file!!' );
			die();
		}

		$this->start_server();
	}

	public function start_server()
	{
		`{$this->defaults->ioquake_path}/ioq3ded.svn.i386 +set dedicated 1 +set net_port 27960 +set com_hunkmegs 56 +exec ctf.cfg +exec maps.cfg`;
	}

	public function update_map_count( $id )
	{
		if ( !$id ) {
			return false;
		}

		$query = $this->db->getQuery( true )
			->update( 'maps' )
			->set( '`count` = `count` + 1' )
			->where( '`id` = ' . (int) $id )
			;
		return $this->db->setQuery( $query )->query();
	}
}

// Wrap the execution in a try statement to catch any exceptions thrown anywhere in the script.
try {
	// Instantiate the application object, passing the class name to JApplicationCli::getInstance
	// and use chaining to execute the application.
	JApplicationCli::getInstance('ServerApp')->execute();
} catch ( Exception $e ) {
	// An exception has been caught, just echo the message.
	fwrite( STDOUT, $e->getMessage() . "\n" );
	exit( $e->getCode() );
}
