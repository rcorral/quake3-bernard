<?php
$path = 'quake3';

if ( !empty( $_POST ) ) {
	$map_count = ( isset( $_POST['map_count'] ) && $_POST['map_count'] )
		? (int) $_POST['map_count'] : 3;
	$sizes = '';

	if ( $map_count > 6 ) {
		$map_count = 6;
	}

	foreach ( array( 's', 'm', 'l', 'g' ) as $size ) {
		if ( isset( $_POST['size_' . $size] ) && $_POST['size_' . $size] ) {
			$sizes .= $size . ',';
		}
	}

	$sizes = substr( $sizes, 0, -1 );

	if ( empty( $sizes ) ) {
		$sizes = 's,m,l';
	}

	`killall ioq3ded.i386`;
	`/usr/local/games/quake3/quake3/scripts/start-server.php {$sizes} {$map_count} > /dev/null 2>&1 &`;

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<title>Server Started</title>
	<link rel="stylesheet" type="text/css" href="/<?php echo $path; ?>/bootstrap/css/bootstrap.min.css" />
</head>
<body>
	<div class="container">
	<div class="hero-unit">
		<h1>Fucking server started.</h1>
	</div>
	</div>
</body>
</html>
<?php
} else {
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<title>Awesomeness</title>
	<link rel="stylesheet" type="text/css" href="/<?php echo $path; ?>/bootstrap/css/bootstrap.min.css" />
</head>
<body>
	<div class="container">
	<div class="hero-unit">
		<h1>Mo Fucking server time</h1>
		<hr />
		<p>
		<form class="form-horizontal" method="post" action="">
			<div class="control-group">
				<label class="control-label" for="map_count">Map count</label>
				<div class="controls">
					<input type="text" placeholder="3" name="map_count" id="map_count" class="span1" /><span class="help-inline">No moar than 6, cuz I said so.</span>
				</div>
			</div>

			<div class="control-group">
				<label class="control-label">Map sizes</label>
				<div class="controls">
					<label class="checkbox inline">
						<input type="checkbox" name="size_s" checked /> Small
					</label>
				</div>
				<div class="controls">
					<label class="checkbox inline">
						<input type="checkbox" name="size_m" checked /> Medium
					</label>
				</div>
				<div class="controls">
					<label class="checkbox inline">
						<input type="checkbox" name="size_l" checked /> Large
					</label>
				</div>
				<div class="controls">
					<label class="checkbox inline">
						<input type="checkbox" name="size_g" /> Giant
					</label>
				</div>
			</div>

			<div class="control-group">
				<div class="controls">
					<button type="submit" class="btn btn-primary">Start le server</button>
				</div>
			</div>
		</form>
		</p>
	</div>
	</div>
</body>
</html>
<?php } ?>